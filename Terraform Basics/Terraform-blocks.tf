
## this block is meant to set a constraint on Terraform version
terraform {
  required_version = ">=1.1.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

# /* # PROVIDER block--- authentication and authorization
# provider "aws" {
#  region     = "us-west-2"
#  access_key = "my-access-key"
#   secret_key = "my-secret-key"
# } */
provider "aws" {
    region = "us-east-1"
  profile = "default"
}

#RESOURCE block - used to CREATE
resource "aws_vpc" "main" {
  cidr_block       = var.vpc_cidr
  


  tags = {
    Name = "main"
  }
}

resource "aws_vpc" "mainnew" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "mainnew"
  }
}

##Variable
variable "ami_id" {
  type = string
  description = "ami_id"
  default = "ami-09d3b3274b6c5d4aa"
  
}
##variable 
variable "instance_type" {
  type = string
  description = "instance_type"
  default = "t2.micro"
  
}



##Creating EC2 Instance

resource "aws_instance" "ec2_TERRA" {
  ami           = data.aws_ami.amazon_linux.id
  instance_type = "t2.micro"

   tags = {
    Name = "ec2_TERRA"
  }
    }

  #   resource "aws_instance" "ec2_TERRA" {
  # ami           = "ami-09d3b3274b6c5d4aa" # us-west-2
  # instance_type = "t2.micro"

  #  tags = {
  #   Name = "ec2_TERRA"
  # }
  #   }
### to fetch logical attibutes
resource "aws_subnet" "main" {
  vpc_id     = aws_vpc.main.id
  cidr_block = "10.0.1.0/24"

  tags = {
    Name = "Main"
  }
}


#output block
output "private_dns" {
  description = "private dns id"
  value = "aws_instance.ec2_TERRA.private_dns"
}


    ##Data source block
    # used to pull down (show) atrribute of existing resources
    data "aws_availability_zones" "available" {
  state = "available"
}


# data.aws_availability_zones.available.names[0] to list
#Data types
# "" - String
#[] list
#80 -Number
#true/false - Boolean
#{} - Map