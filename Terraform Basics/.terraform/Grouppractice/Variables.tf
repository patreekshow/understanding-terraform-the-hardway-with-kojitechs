
variable "ami_linux" {
    type = string
    description = "ami_id"
    default = "ami-09d3b3274b6c5d4aa"
}


variable "groupec2" {
    type = string
    description = "ec2"
    default = "t2.micro"
}

variable "cidr_block-private" {
    type = string
    description = "cidrblockprivate"
    default = "10.0.3.0/24"
}

variable "cidr_block-public" {
    type = string
    description = "cidrblockpublic"
    default = "10.0.1.0/24"
}


variable "az_public" {
    type = string
    description = "az.public"
    default = "us-east-1a"
}

variable "az_private" {
    type = string
    description = "az.private"
    default = "us-east-1b"
}