
resource "aws_instance" "GroupEC2" {
  ami           = var.ami_linux
  instance_type = var.groupec2

  tags = {
    Name = "group_ec2"
  }
}

resource "aws_vpc" "Group_VPC" {
  cidr_block       = "10.0.0.0/16"

  tags = {
    Name = "Module_VPC"
  }
}

resource "aws_subnet" "publicsubnet" {
  vpc_id     = aws_vpc.Group_VPC.id
  cidr_block = var.cidr_block-public
availability_zone = var.az_public
  tags = {
    Name = "grouppublicsubnet"
  }
}

resource "aws_subnet" "privatesubnet" {
  vpc_id     = aws_vpc.Group_VPC.id
  cidr_block = var.cidr_block-private
availability_zone = var.az_private
  tags = {
    Name = "grouprivatesubnet"
  }
}
