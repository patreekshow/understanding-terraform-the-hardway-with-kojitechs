

#Cidr block variable
variable "vpc_cidr" {
  type = string
  description = "cidr_block"
  default = "10.0.0.0/16"
}

variable "Private_subnets_cidr" {
  type = list
  description = "private subnets cidr"
  default =  ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
}

variable "public_subnets_cidr" {
  type = list
  description = "public subnets cidr"
  default = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]
  
}
